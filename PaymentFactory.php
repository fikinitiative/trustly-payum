<?php
namespace fikinitiative\Payum\Trustly;

use Payum\Core\Payment;
use fikinitiative\Payum\Trustly\Action\CaptureAction;
use fikinitiative\Payum\Trustly\Action\StatusAction;

abstract class PaymentFactory
{
    /**
     * @return \Payum\Core\Payment
     **/
    public static function create(\Trustly_Api_Signed $api)
    {
        $payment = new Payment;
        $payment->addApi($api);
        $payment->addAction(new CaptureAction);
        $payment->addAction(new StatusAction);

        return $payment;
    }
}
