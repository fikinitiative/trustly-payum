<?php
namespace fikinitiative\Payum\Trustly\Bridge\Symfony;

use Payum\Bundle\PayumBundle\DependencyInjection\Factory\Payment\AbstractPaymentFactory;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use fikinitiative\Payum\Trustly\Api;

class TrustlyPaymentFactory extends AbstractPaymentFactory
{
    /**
     * @param Definition $paymentDefinition
     * @param ContainerBuilder $container
     * @param $contextName
     * @param array $config
     **/
    protected function addActions(
        Definition $paymentDefinition,
        ContainerBuilder $container,
        $contextName,
        array $config
    ) {
        $captureAction = new Definition;
        $captureAction->setClass('thefik\TrustlyPayum\Action\CaptureAction');
        $captureAction->setPublic(false);
        $captureAction->addTag(
            'payum.action',
            array(
                'factory' => 'trustly'
            )
        );
        $container->setDefinition('thefik.trustly_payum.action.capture', $captureAction);

        $statusAction = new Definition;
        $statusAction->setClass('thefik\TrustlyPayum\Action\StatusAction');
        $statusAction->setPublic(false);
        $statusAction->addTag(
            'payum.action',
            array(
                'factory' => 'trustly'
            )
        );
        $container->setDefinition('thefik.trustly_payum.action.status', $statusAction);
    }

    /**
     * @param Definition $paymentDefinition
     * @param ContainerBuilder $container
     * @param $contextName
     * @param array $config
     **/
    protected function addApis(Definition $paymentDefinition, ContainerBuilder $container, $contextName, array $config)
    {
        $trustly = new Definition;
        $trustly->setClass('Api');
        //$trustly->addArgument($config['api_wsdl']);
        $container->setDefinition('fikinitiative.trustly_payum.api', $trustly);
        $paymentDefinition->addMethodCall('addApi', array(new Reference('fikinitiative.trustly_payum.api')));
    }

    /**
     *      * {@inheritDoc}
     *           */
//    public function addConfiguration(ArrayNodeDefinition $builder)
//    {
//        parent::addConfiguration($builder);
//
//        $builder
//            ->children()
//            ->scalarNode('shop_id')
//            ->isRequired()
//            ->cannotBeEmpty()
//            ->end()
//            ->scalarNode('shop_username')
//            ->isRequired()
//            ->cannotBeEmpty()
//            ->end()
//            ->scalarNode('shop_password')
//            ->isRequired()
//            ->cannotBeEmpty()
//            ->end()
//            ->scalarNode('shop_secret_key')
//            ->isRequired()
//            ->cannotBeEmpty()
//            ->end()
//            ->scalarNode('api_wsdl')
//            ->defaultValue('https://pgw.t-com.hr/MerchantPayment/PaymentWS.asmx?wsdl')
//            ->end()
//            ->scalarNode('api_options_location')
//            ->defaultValue('https://pgw.t-com.hr/MerchantPayment/PaymentWS.asmx')
//            ->end()
//            ->scalarNode('api_options_trace')
//            ->defaultValue('1')
//            ->end()
//            ->scalarNode('api_options_url')
//            ->defaultValue('https://pgw.t-com.hr/MerchantPayment/PaymentWS.asmx')
//            ->end()
//            ->end()
//            ->end();
//    }

    /**
     * {@inheritDoc}
     **/
    public function getName()
    {
        return 'trustly_payment_factory';
    }
}
