<?php
namespace fikinitiative\Payum\Trustly;

class Api
{
    protected $options = array();

    public function __construct(array $options, ClientInterface $client = null)
    {
        $this->client = $client ?: ClientFactory::createCurl();
        $this->options = array_replace( $this->options, $options );
    }

    public function getApi()
    { 
        return new \Trustly_Api_Signed();
    }
}
