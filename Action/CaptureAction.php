<?php
namespace fikinitiative\Payum\Trustly\Action;

use Payum\Core\Action\PaymentAwareAction;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\Capture;
use Payum\Core\Reply\HttpRedirect;

class CaptureAction extends PaymentAwareAction implements ActionInterface, ApiAwareInterface
{
    /**
     * @var Api
     **/
    protected $api;

    /**
     * {@inheritDoc}
     **/
    public function setApi($api)
    {
        if (false === $api instanceof \Trustly_Api_Signed) {
            throw new UnsupportedApiException('Not supported.');
        }
        $this->api = $api;
    }

    /**
     * {@inheritDoc}
     */
    public function execute($request)
    {
        $model = ArrayObject::ensureArrayObject($request->getModel());

        try{
            $deposit = $this->api->deposit(
                $model['notificationurl'],
                $model['enduserid'],
                $model['messageid'],
                $model['locale'],
                $model['amount'],
                $model['currency'],
                $model['country'],
                $model['mobilephone'],
                $model['firstname'],
                $model['lastname'],
                $model['nationalidentificationnumber'],
                $model['shopperstatement'],
                $model['ip'],
                $request->getToken()->getAfterUrl(),
                $model['failurl'],
                $model['templateurl'],
                $model['urltarget'],
                $model['suggestedminamount'],
                $model['suggestedmaxamount'],
                $model['integrationmodule']
            );

            if($deposit->getData('url')) {
                throw new HttpRedirect($deposit->getData('url'));
            } else {
                $model['status'] = 'error';
            }
        } catch (InvalidArgumentException $e) {
            unset($model['auth']);
            $model['status'] = 'Auth Error';

            return;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess
        ;
    }
}
